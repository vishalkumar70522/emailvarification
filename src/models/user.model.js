const mongoose = require('mongoose')

const userSchema = mongoose.Schema({

  email: {
    type: String,
    require: true
  },
  password: {
    type: String,
    require: true
  },
  status: {
    type: String,
    enum: ['Pending', 'Active'],
    default: 'Pending'
  },
  

})

const User = mongoose.model('user', userSchema)
module.exports = User