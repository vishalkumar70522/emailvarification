const express=  require('express')

const router = express.Router()

const {emailVarification,userSingUp, conformEmail, changePassword} = require('../controllers/user.controller')

router.post('/signUp', emailVarification , userSingUp)
router.get('/confirm/:email' , conformEmail)
router.put('/changePassword' , changePassword)
module.exports  = router