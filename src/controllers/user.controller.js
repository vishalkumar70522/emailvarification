const User = require('../models/user.model')
const dotenv = require('dotenv');
dotenv.config();

const nodemailer = require("nodemailer");
const user = 'testgamil4@gmail.com';
const pass = 'cyaannatypsghbuk';

let transporter = nodemailer.createTransport({
    host: "smtp.gmail.com",
    port: 587,
    secure: false,
    auth: {
        user: user,
        pass: pass,
    },
});
const sendConfirmationEmail = (fromEmail, toEmail, confirmationCode) => {
    console.log("Check");
    transporter.sendMail({
        from: user, // sender address
        to: toEmail, // list of receivers
        subject: "Please confirm your account",
        html: `<h1>Email Confirmation</h1>
          <h2>Hello Friend</h2>
          <p>Thank you for signUp. Please confirm your email by clicking on the following link</p>
          <a href=http://localhost:4000/confirm/${toEmail}> Click here</a>
          </div>`,
    })
        .catch(err => console.log(err));
};



const emailVarification = (req, res, next) => {
    console.log("Inside emailVarification ")
    sendConfirmationEmail(user, req.body.email)

    next()

}

const userSingUp = (req, res) => {
    console.log("Inside userSingUp ")
    const email = req.body.email
    const newUser = new User(req.body)
    User.findOne({ email }).exec((err, user) => {
        if (err) {
            console.log("err: ", err)
            res.status(500).send({
                status: 0,
                message: 'User conformation failed!',
                error: err.message
            })
        }
        if (user) {
            res.send({
                status: 0,
                message: "User alread present"
            })
        } else {
            newUser.save()
                .then((data) => {
                    console.log("user Saved : ", data)
                    res.status(201).send({
                        status: 1,
                        message: 'User Created and sent verification link to this ' + req.body.email,
                    })
                })
                .catch((err) => {
                    console.log("not saved")
                    res.status(500).send({
                        status: 0,
                        message: 'User Not saved',
                        error: err.message
                    })
                })
        }
    })
}

const conformEmail = (req, res) => {
    console.log("conform Email called!")
    const email = req.params.email
    User.findOne({ email }).exec((err, user) => {
        if (err) {
            console.log("err: ", err)
            res.status(500).send({
                status: 0,
                message: 'User conformation failed!',
                error: err.message
            })
        }
        if (user) {

            User.updateOne({ email }, { status: "Active" }).exec(err => {
                if (err) {
                    console.log("update err", err)
                    res.status(500).send({
                        status: 0,
                        error: err.message
                    })
                }
                else {
                    console.log("udateded!")
                    res.status(202).send({
                        status: 1,
                        message: 'Email conformed',
                    })
                }
            })
        } else {
            res.status(404).send({
                status: 0,
                message: 'User not found',

            })
        }
    })

}

const changePassword = (req, res) => {
    const { email, oldPassword, newPassword } = req.body
    User.findOne({ email: email, password: oldPassword }).exec((err, user) => {
        if (err) {
            console.log(err)
            res.status(500).send({
                status: 0,
                error: err.message
            })
        }
        if (user) {

            User.updateOne({ email }, { password: newPassword }).exec(err => {
                if (err) {
                    console.log("password update err : ", err)
                    res.status(500).send({
                        status: 0,
                        error: err.message
                    })
                }
                else {
                    console.log('Updated Password')
                    res.send(202, {
                        status: 1,
                        message: 'Password Chaned!',

                    })
                }
            })

        } else {
            console.log('User not found')
            res.status(404).send({
                status: 0,
                message: 'User Not Found!',

            })
        }
    })

}

module.exports = {
    emailVarification,
    userSingUp,
    conformEmail,
    changePassword
}