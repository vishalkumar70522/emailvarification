const express = require('express')
const mongoose = require('mongoose');
const app = express()
const dotenv = require('dotenv');
const x = dotenv.config();
mongoose.connect('mongodb://localhost:27017/assignment');


const userRouter = require('./src/routers/user.router')


app.use(express.json())
app.use('/', userRouter)

app.listen(4000, () => {
    console.log(`Server is running on port: 4000`)
})